from ctypes import util


class BoxContentError(Exception):
    pass

class BoxTooFullError(Exception):
    pass

class Thing:
    def __init__(self, vol,name=None):
        self._volume = vol

    def volume(self):
        return self._volume
    
    def set_name(self,nom):
         self.nom=nom

    def name(self):
        return self.nom

    def __repr__(self):
        return "l'objet "+self.name()+" a un volume de "+str(self.volume())

    def has_name(self,hasname):
        if self.name()==hasname:
            return True
        return False


class Utilisateur :
    def __init__(self,nom_utilisateur,prenom_utilisateur,id_utilisateur) :
        self._nom_U = nom_utilisateur
        self._prenom_U = prenom_utilisateur
        self._id_U = id_utilisateur

    def nom_U(self) :
        return self._nom_U

    def prenom_U(self) :
        return self._prenom_U
    
    def id_U(self) :
        return self._id_U

    def affiche_utilisateur(self) :
        return "L'utilisateur s'appelle "+self.nom_U()+" "+self.prenom_U() + " et possède le numéro d'identifiant "+str(self.id_U())




class Salle :
    def __init__(self,id_createur,nom_salle) :
        self._id_crea = id_createur
        self._nom_s = nom_salle
    
    def id_crea(self) :
        return self._id_crea

    def nom_s(self) :
        return self._nom_s
        

class Box:
    def __init__(self,cap=None,id_crea = None):
        self._contents = []
        self._open = False
        self._capacity = cap

    """
    def retrouve_utilisateur(self,liste_utilisateur): 
        for utilisateur in liste_utilisateur :
            if utilisateur.id_U() == self.id_C :
                return utilisateur
        return None
    """

    def id_C(self) :
        return self._id_C

    def capacity(self):
        return self._capacity

    def set_capacity(self, cap):
        self._capacity = cap

    def is_open(self):
        return self._open

    def open(self):
        self._open = True

    def close(self):
        self._open = False

    def action_add(self, objet):
        if not self.has_room_for(objet):
            raise BoxTooFullError
        self.add(objet)

    def add(self, objet):
        if self._capacity is not None:
            self._capacity -= objet.volume()
        self._contents.append(objet)

    def __contains__(self, objet):
        return objet in self._contents

    def remove(self, objet):
        if objet not in self:
            raise BoxContentError
        self._contents.remove(objet)

    def has_room_for(self, objet):
        if self.capacity() is None:
            return True
        return self.capacity() >= objet.volume()

    def action_look(self):
        if self.is_open():
            enumeration_contenu = ", ".join(self._contents)
            return "La boite contient: " + enumeration_contenu + "."
        else:
            return "La boite est fermée."

    def contient_box(self) :
        res = "La boite contient : "
        for obj in self._contents :

            res = res + obj + ", "
        return res

    def find_objet(self,name):
        if self.is_open():
            for elem in self._contents:
                if elem==name:
                    return elem
        return None

  

        

    
