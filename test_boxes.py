from tp2 import Box, BoxContentError, Thing, BoxTooFullError, Utilisateur

def test_box_create():
    b = Box()

def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")

def test_contains():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    assert "truc1" in b
    assert "machin" not in b

def test_remove():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.remove("truc1")
    assert "truc1" not in b

def test_remove_inexistant():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.remove("truc1")
    assert "truc1" not in b
    try:
        b.remove("machin")
        assert False # On n'atteint pas cette ligne car
                     # une exception est levée
    except BoxContentError:
        pass

def test_box_open_close():
    b = Box()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()

def test_box_action_look():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.open()
    look = b.action_look()
    assert look == "La boite contient: truc1, truc2."
    b.close()
    look = b.action_look()
    assert look == "La boite est fermée."

def test_box_capacity():
    b = Box(5)
    assert b.capacity() == 5
    b.set_capacity(6)
    assert b.capacity() == 6

def test_create_thing():
    t = Thing(3)
    assert t.volume() == 3

def test_has_room_for():
    b = Box(5)
    t3 = Thing(3)
    t2 = Thing(2)
    t4 = Thing(4)
    b.add(t3)
    assert b.has_room_for(t2)
    assert not b.has_room_for(t4)

def test_add_capacity():
    b = Box(5)
    t3 = Thing(3)
    t2 = Thing(2)
    t4 = Thing(4)
    b.action_add(t3)
    try:
        b.action_add(t4)
        assert False, "erreur, on ne doit pas pouvoir mettre cet objet"
    except BoxTooFullError:
        pass
    b.action_add(t2) # on peut ajouter t2 car l'ajout de t4 n'a pas été fait
                     # donc il reste de la place.


def test_create_utilisateur() :
    u = Utilisateur("Teton","Michel",41)
    assert u.nom_U() == "Teton"
    assert u.id_U() == 41
    assert u.prenom_U() == "Michel"

def test_affiche_box() :
    b12 = Box()
    b12.add("Gilet")
    b12.add("PS5")
    b12.add("Fourchette")
    b12.add("Ange olive heure")
    b12.add("Pale de sous marins")
    b12.open()
    assert b12.contient_box() == "La boite contient : Gilet, PS5, Fourchette, Ange olive heure, Pale de sous marins, "


def test_affiche_utilisateur() :
    u = Utilisateur("Souris","Charlie", 18)
    u.affiche_utilisateur() == "L'utilisateur s'appelle Souris Charlie et possède le numéro d'identifiant 18"


def test_cree_salle() :
    u = Utilisateur ("Custo", "Richard", 13)

                
def test_affiche_objet():
    t3=Thing(5)
    t3.set_name("bidulle")
    assert t3.name()=="bidulle"
    assert str(t3)=="l'objet bidulle a un volume de 5"

def test_hasname():
    t1=Thing(5)
    t1.set_name("bidulle")
    t2=Thing(10)
    t2.set_name("machin")
    t3=Thing(6)
    t3.set_name("truc")
    assert t1.has_name("pomme de terre")==False
    assert t2.has_name("machin")==True
    assert t3.has_name("ballon")==False

def test_find():
    b1 = Box()
    b1.open()
    b1.add("cafard")
    b1.add("souris")
    b1.add("téléphone")
    b2 = Box()
    b2.add("IBCONNECT")
    b2.add("interrupteur")
    b2.add("terroriste")
    b3 = Box()
    b3.open()
    b3.add("antagoniste")
    b3.add("sénateur")
    b3.add("pull")
    assert b1.find_objet("cafard")=="cafard"
    assert b2.find_objet("interrupteur")==None
    assert b3.find_objet("souris")==None


